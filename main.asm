; R0 Counter
; R1 First digit
; R2 Second digit
; P1 Digits input
; P3.4 .. P3.7 Digits select
; P0.0 Interrupt reset

; Make sure to reset interrupt manually if IDE starts bugging at full speed
; Use P0.0 to jump back from interrupt to main loop (countdown)

org 0000h
ljmp initialize

org 0003h
ljmp onInterrupt

onInterrupt:
lcall keepdisplayalive
mov A,P0
; bitmask 1000 0000
ANL A,#80h
cjne A,#0,onInterrupt
mov R0,#0Ah
reti

initialize:
mov IE,#81h
mov R0,#0Ah

main:
; check counter
cjne R0, #0, iterate
; reset
mov R0,#0Ah
sjmp main

iterate:
; decrement counter
dec R0
; get first and second digit
mov A,R0
mov B,#0Ah
div AB
; get second digit
mov DPTR, #table
movc A,@a+dptr
mov R2,A
; get first digit using remainder
mov A,B
mov DPTR, #table
movc A,@a+dptr
mov R1,A
; display numbers
sjmp display

display:
; first digit
mov P1, R1
clr P3.4
setb P3.4
; second digit
mov P1, R2
clr P3.5
setb P3.5
sjmp main

keepDisplayAlive:
; check if selected value
mov A,P0
; get complement
cpl A
; bit mask 0111 1111
ANL A,#7Fh
cjne A,0,keepdisplayalivelost
; won => light up display
mov P1,#0
clr P3.4
clr P3.5
clr P3.6
clr P3.7
setb P3.4
setb P3.5
setb P3.6
setb P3.7
ret

keepDisplayAliveLost:
; first digit
mov P1, R1
clr P3.4
setb P3.4
; second digit
mov P1, R2
clr P3.5
setb P3.5
ret

table:
db 11000000b, 11111001b, 10100100b, 10110000b, 10011001b, 10010010b, 10000010b, 11111000b, 10000000b, 10010000b

end